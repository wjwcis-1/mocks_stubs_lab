package loginservice;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class LoginServiceTest {
    private IAccount account;
    private IAccountRepository accountRepository;
    private LoginService service;

    @Before
    public void init() {
        account = mock(IAccount.class);
        when(account.getId()).thenReturn("brett");
        accountRepository = mock(IAccountRepository.class);
        when(accountRepository.find(anyString())).thenReturn(account);
        service = new LoginService(accountRepository);
    }

    private void willPasswordMatch(boolean value) {
        when(account.passwordMatches(anyString())).thenReturn(value);
    }
    private void willPasswordCurrent(boolean value){
        when(account.passwordCurrent(anyString())).thenReturn(value);
    }
    private void willPasswordValid(boolean value){
        when(account.passwordValid(anyString())).thenReturn(value);
    }
    private void willGreaterThan24(boolean value){
        when(account.GreatThan24(anyString())).thenReturn(value);
    }

    @Test
    public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
        willPasswordMatch(true);
        willPasswordValid(true);
        willPasswordCurrent(true);
        willGreaterThan24(true);
        service.login("brett", "password");
        verify(account, times(1)).setLoggedIn(true);
    }

    @Test
    public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
        willPasswordMatch(false);
        willPasswordValid(false);
        willPasswordCurrent(false);
        willGreaterThan24(false);

        for (int i = 0; i < 3; ++i)
            service.login("brett", "password");

        verify(account, times(1)).setRevoked(true);
    }

    @Test
    public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
        willPasswordMatch(false);
        willPasswordValid(false);
        willPasswordCurrent(false);
        willGreaterThan24(false);
        service.login("brett", "password");
        verify(account, never()).setLoggedIn(true);
    }

    @Test
    public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
        willPasswordMatch(false);
        willPasswordValid(false);
        willPasswordCurrent(false);
        willGreaterThan24(false);

        IAccount secondAccount = mock(IAccount.class);
        when(secondAccount.passwordMatches(anyString())).thenReturn(false);
        when(accountRepository.find("schuchert")).thenReturn(secondAccount);

        service.login("brett", "password");
        service.login("brett", "password");
        service.login("schuchert", "password");

        verify(secondAccount, never()).setRevoked(true);
    }

    @Test(expected = AccountLoginLimitReachedException.class)
    public void itShouldNowAllowConcurrentLogins() {
        willPasswordMatch(true);
        willPasswordValid(true);
        willPasswordCurrent(true);
        willGreaterThan24(true);
        when(account.isLoggedIn()).thenReturn(true);
        service.login("brett", "password");
    }

    @Test(expected = AccountNotFoundException.class)
    public void itShouldThrowExceptionIfAccountNotFound() {
        when(accountRepository.find("schuchert")).thenReturn(null);
        service.login("schuchert", "password");
    }

    @Test(expected = AccountRevokedException.class)
    public void ItShouldNotBePossibleToLogIntoRevokedAccount() {
        willPasswordMatch(true);
        willPasswordValid(true);
        willPasswordCurrent(true);
        willGreaterThan24(true);
        when(account.isRevoked()).thenReturn(true);
        service.login("brett", "password");
    }

    @Test
    public void itShouldResetBackToInitialStateAfterSuccessfulLogin() {
        willPasswordMatch(false);
        willPasswordCurrent(false);
        willPasswordValid(false);
        willGreaterThan24(false);
        service.login("brett", "password");
        service.login("brett", "password");
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(true);
        willGreaterThan24(true);
        service.login("brett", "password");
        willPasswordMatch(false);
        willPasswordCurrent(false);
        willPasswordValid(false);
        willGreaterThan24(false);
        service.login("brett", "password");
        verify(account, never()).setRevoked(true);
    }
    @Test
    public void itCannotLogAccountWithExpiredPassword(){
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(false);
        willGreaterThan24(true);
        service.login("brett", "password");
        verify(account, never()).setLoggedIn(true);
    }
    //Cannot Login to Account with Expired Password
    @Test
    public void itCanLogIntoAccountWithExpiredPasswordAfterChangingThePassword(){
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(false);
        willGreaterThan24(true);
        service.login("brett", "password");
        verify(account, never()).setLoggedIn(true);
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(true);
        willGreaterThan24(true);
        service.login("brett","123");
        verify(account, times(1)).setLoggedIn(true);
    }
    //Can Login to Account with Expired Password After Changing the Password
    @Test
    public void itCannotLogIntoAccountWithTemporaryPassword(){
        willPasswordMatch(true);
        willPasswordCurrent(false);
        willPasswordValid(true);
        willGreaterThan24(true);
        service.login("brett", "temp");
        verify(account, never()).setLoggedIn(true);
    }
    //Cannot Login to Account with Temporary Password
    @Test
    public void itCanLoginAccountWithTemporaryPasswordAfterChangingPassword(){
        willPasswordMatch(true);
        willPasswordCurrent(false);
        willPasswordValid(true);
        willGreaterThan24(true);
        service.login("brett", "temp");
        verify(account, never()).setLoggedIn(true);
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(true);
        willGreaterThan24(true);
        service.login("brett","123");
        verify(account, times(1)).setLoggedIn(true);
    }
    //Can Login to Account with Temporary Password After Changing Password
    @Test
    public void itCannotChangePasswordAnyOfPrevious24passwords(){
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(true);
        willGreaterThan24(false);
        service.login("brett", "temp");
        verify(account, never()).setLoggedIn(true);
        verify(account, never()).setCanChangePassword(true);
    }
    //Cannot Change Password to any of Previous 24 passwords
    @Test
    public void itCanChangePasswordPreviousPasswordIfGreaterThan24ChangesFromLastUse(){
        willPasswordMatch(true);
        willPasswordCurrent(true);
        willPasswordValid(true);
        willGreaterThan24(true);
        service.login("brett", "temp");
        verify(account, times(1)).setLoggedIn(true);
        verify(account, times(1)).setCanChangePassword(true);
    }
//Can Change Password to Previous password if > 24 Changes from last use

}