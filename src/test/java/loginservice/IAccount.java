package loginservice;

public interface IAccount {
    boolean passwordCurrent(String password);
    boolean passwordValid(String password);
    boolean passwordMatches(String password);
    boolean GreatThan24(String password);
    void setLoggedIn(boolean value);
    void setRevoked(boolean value);
    void setCanChangePassword(boolean value);
    boolean isLoggedIn();
    boolean isRevoked();
    boolean CanChangePassword();
    String getId();
}
