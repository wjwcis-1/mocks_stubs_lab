package loginservice;

public abstract class LoginServiceState {
    public final void login(LoginServiceContext context, IAccount account,
                            String password) {
        if (account.passwordMatches(password)
                && account.passwordCurrent(password)
                && account.passwordValid(password)
                && account.GreatThan24(password)) {
            if (account.isLoggedIn())
                throw new AccountLoginLimitReachedException();
            if (account.isRevoked())
                throw new AccountRevokedException();
            if (account.CanChangePassword())
                throw new PasswordCanNotBeChangeException();
            account.setLoggedIn(true);
            account.setCanChangePassword(true);
            context.setState(new AwaitingFirstLoginAttempt());
        } else
            handleIncorrectPassword(context, account, password);
    }

    public abstract void handleIncorrectPassword(LoginServiceContext context,
                                                 IAccount account, String password);
}